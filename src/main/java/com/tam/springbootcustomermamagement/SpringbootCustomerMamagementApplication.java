package com.tam.springbootcustomermamagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootCustomerMamagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootCustomerMamagementApplication.class, args);
	}

}
