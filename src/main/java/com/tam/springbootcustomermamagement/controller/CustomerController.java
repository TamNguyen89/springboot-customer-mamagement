package com.tam.springbootcustomermamagement.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CustomerController {

    @GetMapping("/index")
    public String index(Model model) {
        model.addAttribute("message", "Welcome to spring thymeleaf");
        return "index";
    }
}
