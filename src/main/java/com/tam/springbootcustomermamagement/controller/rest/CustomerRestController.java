package com.tam.springbootcustomermamagement.controller.rest;

import com.tam.springbootcustomermamagement.request.CreateCustomerRequest;
import com.tam.springbootcustomermamagement.request.UpdateCustomerRequest;
import com.tam.springbootcustomermamagement.response.CustomerResponse;
import com.tam.springbootcustomermamagement.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/customers")
public class CustomerRestController {

    @Autowired
    private CustomerService customerService;

    @GetMapping
    public List<CustomerResponse> getAllCustomers() {
        return customerService.getAllCustomers();
    }

    @GetMapping("/{id}")
    public CustomerResponse getbyId(@PathVariable long id) {
        return customerService.getbyId(id);
    }

    @PostMapping
    public ResponseEntity create(@RequestBody CreateCustomerRequest request) {
        customerService.create(request);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public void update(@PathVariable long id,
                       @RequestBody UpdateCustomerRequest request) {
        customerService.update(id, request);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id) {
        customerService.delete(id);
    }



}
