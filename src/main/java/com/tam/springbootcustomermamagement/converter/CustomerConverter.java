package com.tam.springbootcustomermamagement.converter;

import com.tam.springbootcustomermamagement.entity.CustomerEntity;
import com.tam.springbootcustomermamagement.request.CreateCustomerRequest;
import com.tam.springbootcustomermamagement.response.CustomerResponse;

public class CustomerConverter {

    public static CustomerResponse convert(CustomerEntity entity) {
        return CustomerResponse.builder()
                                .id(entity.getId())
                                .firstname(entity.getFirstName())
                                .lastname(entity.getLastName())
                                .build();
    }

    public static CustomerEntity convert(CreateCustomerRequest request) {
        CustomerEntity entity = new CustomerEntity();
        entity.setFirstName(request.getFirstName());
        entity.setLastName(request.getLastName());
        return entity;
    }
}
