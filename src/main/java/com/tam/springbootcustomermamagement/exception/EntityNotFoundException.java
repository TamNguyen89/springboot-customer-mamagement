package com.tam.springbootcustomermamagement.exception;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class EntityNotFoundException extends RuntimeException {

    String message;

    public EntityNotFoundException(String message) {
        super(message);
        this.message = message;
    }
}
