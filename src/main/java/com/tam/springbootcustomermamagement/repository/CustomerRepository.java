package com.tam.springbootcustomermamagement.repository;

import com.tam.springbootcustomermamagement.entity.CustomerEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<CustomerEntity, Long> {
}
