package com.tam.springbootcustomermamagement.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateCustomerRequest {

    String firstName;
    String lastName;
}
