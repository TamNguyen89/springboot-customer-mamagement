package com.tam.springbootcustomermamagement.service;

import com.tam.springbootcustomermamagement.request.CreateCustomerRequest;
import com.tam.springbootcustomermamagement.request.UpdateCustomerRequest;
import com.tam.springbootcustomermamagement.response.CustomerResponse;

import java.util.List;

public interface CustomerService {

    List<CustomerResponse> getAllCustomers();

    CustomerResponse getbyId(long id);

    void create(CreateCustomerRequest request);

    void update(long id, UpdateCustomerRequest request);

    void delete(long id);
}


