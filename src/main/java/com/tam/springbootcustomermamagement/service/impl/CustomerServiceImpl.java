package com.tam.springbootcustomermamagement.service.impl;

import com.tam.springbootcustomermamagement.converter.CustomerConverter;
import com.tam.springbootcustomermamagement.entity.CustomerEntity;
import com.tam.springbootcustomermamagement.exception.EntityNotFoundException;
import com.tam.springbootcustomermamagement.repository.CustomerRepository;
import com.tam.springbootcustomermamagement.request.CreateCustomerRequest;
import com.tam.springbootcustomermamagement.request.UpdateCustomerRequest;
import com.tam.springbootcustomermamagement.response.CustomerResponse;
import com.tam.springbootcustomermamagement.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public List<CustomerResponse> getAllCustomers() {
        return customerRepository.findAll().stream().map(CustomerConverter::convert).collect(Collectors.toList());
    }

    @Override
    public CustomerResponse getbyId(long id) {
        return customerRepository.findById(id).map(CustomerConverter::convert)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Customer with id: %s not found", id)));
    }

    @Override
    public void create(CreateCustomerRequest request) {
        customerRepository.save(CustomerConverter.convert(request));
    }

    @Override
    @Transactional
    public void update(long id, UpdateCustomerRequest request) {
        CustomerEntity entity = customerRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Customer with id: %s not found", id)));
        entity.setFirstName(request.getFirstName());
        entity.setLastName(request.getLastName());
    }

    @Override
    public void delete(long id) {
        customerRepository.deleteById(id);
    }
}
